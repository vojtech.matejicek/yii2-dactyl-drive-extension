<?php

namespace vojtechmatejicek\dactyldrive;

require __DIR__ . '/../../../vendor/autoload.php';

class DactylDrive
{

    public $app_name = 'Google Drive API PHP Quickstart';
    public $scopes = \Google_Service_Drive::DRIVE;
    public $credentials_path = __DIR__ . '/credentials.json';
    public $access = 'offline';
    public $prompt = 'select_account consent';
    public $redirect_uri = 'http://koleso.dgrp.cz';

    public function getService()
    {
        return new \Google_Service_Drive($this->getClient());
    }

    /**
     * Returns an authorized API client.
     * @return \Google_Client
     * @throws \Google_Exception
     */
    public function getClient()
    {
        $client = new \Google_Client();
        $client->setApplicationName($this->app_name);
        $client->setScopes($this->scopes);
        $client->setAuthConfig($this->credentials_path);
        $client->setAccessType($this->access);
        $client->setPrompt($this->prompt);
        $client->setRedirectUri($this->redirect_uri);
        $client->setDeveloperKey("AIzaSyDquv9FJGZ-YVWPl2_dPgQ7qolodGvi5Dg");

        // Load previously authorized token from a file, if it exists.
        // The file token.json stores the user's access and refresh tokens, and is
        // created automatically when the authorization flow completes for the first
        // time.
        $tokenPath = __DIR__ . '/token.json';
        if (file_exists($tokenPath)) {
            $accessToken = json_decode(file_get_contents($tokenPath), true);
            $client->setAccessToken($accessToken);
        }

        // If there is no previous token or it's expired.
        if ($client->isAccessTokenExpired()) {
            // Refresh the token if possible, else fetch a new one.
            if ($client->getRefreshToken()) {
                $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
            } else {
                // Request authorization from the user.
                $authUrl = $client->createAuthUrl();
                printf("Open the following link in your browser:\n%s\n", $authUrl);
                print 'Enter verification code: ';
                $authCode = trim(fgets(STDIN));

                // Exchange authorization code for an access token.
                $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
                $client->setAccessToken($accessToken);

                // Check to see if there was an error.
                if (array_key_exists('error', $accessToken)) {
                    throw new \Exception(join(', ', $accessToken));
                }
            }
            // Save the token to a file.
            if (!file_exists(dirname($tokenPath))) {
                mkdir(dirname($tokenPath), 0700, true);
            }
            file_put_contents($tokenPath, json_encode($client->getAccessToken()));
        }
        return $client;
    }

    /**
     * Checks if folder with given name exists.
     * @param $name
     * @return bool|mixed
     */
    public function folderExists($name, $parent = null)
    {
        $folders = $this->findFolders($name, $parent);
        if (sizeof($folders) > 0) {
            return $folders[0];
        } else {
            return false;
        }
    }

    /**
     * Creates folder with given name if it does not exist.
     * Otherwise returns first folder with given name.
     * @param $name
     * @return mixed
     */
    public function addFolder($name, $parent = null)
    {
        $folder = $this->folderExists($name, $parent);

        $metadata = [
            'name' => $name,
            'mimeType' => 'application/vnd.google-apps.folder',
        ];

        if (!is_null($parent)) {
            $metadata['parents'] = [$parent];
        }

        if (!$folder) {
            $folderMetadata = new \Google_Service_Drive_DriveFile($metadata);

            $folder = $this->getService()->files->create($folderMetadata, array(
                'fields' => 'id, parents'));
        }

        return $folder->id;
    }

    /**
     * Creates folder path
     * @param $folders
     * @return mixed|null
     * @throws \yii\web\HttpException
     */
    public function addPath($folders)
    {
        $folders = explode('/', trim($folders, '/'));

        if (sizeof($folders) < 1) {
            throwException('No folder defined!');
        } elseif (sizeof($folders) == 1) {
            return $this->addFolder($folders[0]);
        } else {
            $parentId = null;
            foreach ($folders as $f => $folderName) {
                $parentId = $this->addFolder($folderName, $parentId);

            }
            return $parentId;
        }
    }

    /**
     * Returns array of existing folders with given name and parent name.
     * @param $name
     * @param null $parent
     * @return \Google_Service_Drive_DriveFile
     */
    public
    function findFolders($name, $parent = null)
    {
        $pageToken = null;
        $query = "mimeType='application/vnd.google-apps.folder' and name='$name' and not trashed";

        if (!is_null($parent))
            $query .= " and '$parent' in parents";
        else
            $query .= " and 'root' in parents";

        do {
            $response = $this->getService()->files->listFiles([
                'q' => $query,
                'spaces' => 'drive',
                'pageToken' => $pageToken,
                'fields' => 'nextPageToken, files(*)',
            ]);

            $pageToken = $response->pageToken;
        } while ($pageToken != null);

        return $response->getFiles();
    }

    /**
     * Uploads file to folder with given path
     * @param $fileData
     * @param $name
     * @param $path
     * @param string $mimeType
     * @return mixed
     * @throws \yii\web\HttpException
     */
    public
    function uploadFileToPath($fileData, $name, $path, $mimeType = 'application/octet-stream')
    {
        if(!is_null($path))
            $folderId = $this->addPath($path);
        else
            $folderId = 'root';

        $name = $this->renameDuplicityFile($name, $folderId);

        $fileMetadata = new \Google_Service_Drive_DriveFile([
            'name' => $name,
            'parents' => [
                $folderId,
            ]
        ]);

        $file = $this->getService()->files->create(
            $fileMetadata,
            [
                'data' => $fileData,
                'mimeType' => $mimeType,
                'uploadType' => 'multipart',
            ]
        );

        return $file->id;
    }


    /**
     * Uploads file to root folder.
     * @param $fileData
     * @param $name
     * @param string $mimeType
     * @return mixed
     */
    public
    function uploadFile($fileData, $name, $mimeType = 'application/octet-stream')
    {
        $file = new \Google_Service_Drive_DriveFile();
        $file->setName($name);

        $this->getService()->files->create(
            $file,
            [
                'data' => $fileData,
                'mimeType' => $mimeType,
                'uploadType' => 'multipart'
            ]
        );

        return $file->id;
    }

    /**
     * Searches for file in given parent folder by file name
     * @param $name
     * @param null $parent
     * @return \Google_Service_Drive_DriveFile
     */
    public function findFile($name, $parent = null)
    {
        $pageToken = null;
        $query = "name='$name' and not trashed";

        if (!is_null($parent))
            $query .= " and '$parent' in parents";
        else
            $query .= " and 'root' in parents";

        do {
            $response = $this->getService()->files->listFiles([
                'q' => $query,
                'spaces' => 'drive',
                'pageToken' => $pageToken,
                'fields' => 'nextPageToken, files(*)',
            ]);

            $pageToken = $response->pageToken;
        } while ($pageToken != null);

        return $response->getFiles();
    }

    /**
     * Prevents uploading files with duplicate names by adding suffix _%d
     * @param $name
     * @param null $parent
     * @return string
     */
    public function renameDuplicityFile($name, $parent = null)
    {
        $tempName = explode('.', $name);

        $pageToken = null;
        $query = "name contains '$tempName[0]' and not trashed";

        if (!is_null($parent))
            $query .= " and '$parent' in parents";
        else
            $query .= " and 'root' in parents";

        do {
            $response = $this->getService()->files->listFiles([
                'q' => $query,
                'spaces' => 'drive',
                'pageToken' => $pageToken,
                'fields' => 'nextPageToken, files(*)',
            ]);

            $pageToken = $response->pageToken;
        } while ($pageToken != null);

//        dieArray($response->getFiles());

        if (empty($response->getFiles())) {
            return $name;
        }

        $suffix = 1;
        $files = $response->getFiles();

        foreach ($files as $f => $file) {
            if (preg_match('/' . $tempName[0] . '_[\d+]?/', $file->getName())) {
                $suffix++;
            }
        }

        return $tempName[0] . "_$suffix." . (isset($tempName[1]) ? $tempName[1] : '');
    }
}